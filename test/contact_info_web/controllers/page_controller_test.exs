defmodule ContactInfoWeb.PageControllerTest do
  use ContactInfoWeb.ConnCase

  alias ContactInfoWeb.Endpoint
  alias ContactInfoWeb.Router.Helpers, as: Routes

  alias ContactInfo.{Repo, Contact}

  test "GET / | Missing case id", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 400) =~ "Missing required parameter"
  end

  test "GET / | Valid case id", %{conn: conn} do
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    assert html_response(conn, 200) =~ "XYZ"
  end

  test "GET / | Too long case id", %{conn: conn} do
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: String.duplicate("a", 256)))
    assert html_response(conn, 400) =~ "too long"
  end

  test "GET / | Empty case id", %{conn: conn} do
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: ""))
    assert html_response(conn, 400) =~ "case_id can not be empty"
  end

  test "GET / | Case with some contacts should show these contacts", %{conn: conn} do
    Repo.insert(%Contact{case_id: "XYZ", first_name: "Martin", address: "Martinstraße 8"})

    Repo.insert(%Contact{
      case_id: "XYZ",
      title: "Mrs",
      last_name: "Miller",
      mobile_number: "(1) 012345"
    })

    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    response = html_response(conn, 200)

    assert response =~ "Martin"
    assert response =~ "Martinstraße 8"
    assert response =~ "Mrs"
    assert response =~ "Miller"
    assert response =~ "(1) 012345"
  end

  test "GET / | Case should not display contacts from other cases", %{conn: conn} do
    Repo.insert(%Contact{case_id: "XYZ", first_name: "Martin", address: "Martinstraße 8"})

    Repo.insert(%Contact{
      case_id: "ABC",
      title: "Mrs",
      last_name: "Miller",
      mobile_number: "(1) 012345"
    })

    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    response = html_response(conn, 200)

    assert response =~ "Martin"
    assert response =~ "Martinstraße 8"
    assert not (response =~ "Mrs")
    assert not (response =~ "Miller")
    assert not (response =~ "(1) 012345")
  end

  test "POST / | We should be able to see form we just posted", %{conn: conn} do
    conn =
      post(
        conn,
        Routes.page_path(Endpoint, :create, contact: %{case_id: "XYZ", first_name: "Max"})
      )

    assert html_response(conn, 302)
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    assert html_response(conn, 200) =~ "Max"
  end

  test "POST / | We should not be able to post to /create without a contact", %{conn: conn} do
    conn = post(conn, Routes.page_path(Endpoint, :create))
    assert html_response(conn, 400) =~ "Missing required parameter"
  end

  test "POST / | We should get an error if we try to post a contact without a case_id", %{
    conn: conn
  } do
    conn = post(conn, Routes.page_path(Endpoint, :create, contact: %{"first_name" => "Max"}))
    assert html_response(conn, 400) =~ "Missing required parameter"
  end

  test "DELETE / | We should not see a contact that was there previously after we deleted it", %{
    conn: conn
  } do
    {:ok, contact} = Repo.insert(%Contact{case_id: "XYZ", first_name: "Max"})

    # this weird construct is because when converting structs Phoenix helpfully just
    # takes the id field and nothing else, which happens to not be what we want in this case
    contact_data = contact |> Map.from_struct() |> Map.take([:id, :case_id])

    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    assert html_response(conn, 200) =~ "Max"
    conn = delete(conn, Routes.page_path(Endpoint, :delete, contact: contact_data))
    assert html_response(conn, 302)
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    assert not (html_response(conn, 200) =~ "Max")
  end

  test "DELETE / | Deleting the same entry twice should be ok", %{conn: conn} do
    {:ok, contact} = Repo.insert(%Contact{case_id: "XYZ", first_name: "Max"})
    # this weird construct is because when converting structs Phoenix helpfully just
    # takes the id field and nothing else, which happens to not be what we want in this case
    contact_data = contact |> Map.from_struct() |> Map.take([:id, :case_id])
    conn = delete(conn, Routes.page_path(Endpoint, :delete, contact: contact_data))
    assert html_response(conn, 302)
    conn = delete(conn, Routes.page_path(Endpoint, :delete, contact: contact_data))
    assert html_response(conn, 302)
  end

  test "DELETE / | Deleting an entry with an invalid id gives an error", %{conn: conn} do
    conn =
      delete(conn, Routes.page_path(Endpoint, :delete, contact: %{case_id: "XYZ", id: "invalid"}))

    assert html_response(conn, 400) =~ "must be integer"
  end

  test "DELETE / | Deleting an entry with missing id gives an error", %{conn: conn} do
    conn = delete(conn, Routes.page_path(Endpoint, :delete, contact: %{case_id: "XYZ"}))
    assert html_response(conn, 400) =~ "must have an id"
  end

  test "GET /edit | Trying to look at a contact that doesn't exist yields a 404", %{conn: conn} do
    conn = get(conn, Routes.page_path(Endpoint, :edit_get, contact_id: 9999))
    assert html_response(conn, 404)
  end

  test "GET /edit | Looking at an existing contact gives us the contact information", %{
    conn: conn
  } do
    {:ok, contact} = Repo.insert(%Contact{case_id: "XYZ", first_name: "Max"})
    conn = get(conn, Routes.page_path(Endpoint, :edit_get, contact_id: contact.id))
    assert html_response(conn, 200) =~ "Max"
  end

  test "PUT / | Changing an existing field is reflected in next get of index", %{conn: conn} do
    max = %{case_id: "XYZ", first_name: "Maks"}
    {:ok, contact} = Repo.insert(struct(Contact, max))
    max_updated = max |> Map.put(:id, contact.id) |> Map.put(:first_name, "Max")
    conn = put(conn, Routes.page_path(Endpoint, :edit_put, contact: max_updated))
    assert html_response(conn, 302)
    conn = get(conn, Routes.page_path(Endpoint, :index, case_id: "XYZ"))
    assert html_response(conn, 200) =~ "Max"
  end

  test "Authentication: Accessing index without authorization gives an error", %{conn: conn} do
    conn =
      conn
      |> delete_req_header("authorization")
      |> get(Routes.page_path(Endpoint, :index, case_id: "XYZ"))

    assert text_response(conn, 403) =~ "forbidden"
  end

  test "Authentication: Accessing index with an invalid authorization gives an error", %{
    conn: conn
  } do
    # I got this from jwt.io, this is signed with HS256 instead of RS256
    invalid_jwt_token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"

    conn =
      conn
      |> put_req_header("authorization", invalid_jwt_token)
      |> get(Routes.page_path(Endpoint, :index, case_id: "XYZ"))

    assert text_response(conn, 403) =~ "forbidden"
  end

  test "Authentication: Accessing index with an authorization signed by the wrong key gives an error",
       %{conn: conn} do
    # Got this one from jwt.io too, this one is the same as the test token but signed with a different signature
    invalid_jwt_token =
      "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJwYXJjLWNsaSIsInN1YiI6InRlc3RfdG9rZW4iLCJpYXQiOjE2MTA3MDE3Njh9.M26whN_tQOCNBxsrC2IB3qF1gCVXB4aysRH70ZOZ6SIdF02-m83pNYlDOpA-cTobxAFhUT3j-_6-uN_uNcdcYT_plDB8bbXK76bE3ObTcaW-H9LJz87e_HaYqg4yJAR1v-2YLALWgzHVvYnfeofG5XR_jswNMO87Q-c8xKsVO9rkpSJRZwMow7CJwCTizJMX2_nE-abpO26S0IN1ZLrVGZseOtobgJD3Dk7fkikm2tGITXPuIQe5veRhpdd82ejK8TTLBIxys_5G_abml3B7bcG00oCty9tgFkYw76nfFQqQndjgVQbHqqgZjXJjBdYEs95iScHBoYwmcoL1Ue-0IJID5k99QfqwJLXx3jfsvA60QHmDVNhAoJR-o7QqYohLmnZl3CNv9LUo991SYrj4xGWYHZG0vAXiJT0krzbWoVszlMBvgFLEKq_-lVpn9_MyUAiSj5wYuwEYXcuz6OzoX6lfWuNlYyzKeAgdTYseZuhqu7Vju4_j-fNthMu-P4pXCUhi9JV0SKzbkaw9IFkda9N_uLg_UNjAb9NFSch9wEwAX-3opPQ4vLTWnMcT3msPsnT8JPRgt0tPqdXuYvktY0T4G_tCLQ0_XD06fHy74USdOeuKONdDSHUwHSEx6WNyx2sIubQp2UdafFTxNW2XClqulI9apUc3HcBHfQokBig"

    conn =
      conn
      |> put_req_header("authorization", invalid_jwt_token)
      |> get(Routes.page_path(Endpoint, :index, case_id: "XYZ"))

    assert text_response(conn, 403) =~ "forbidden"
  end
end
