defmodule ContactInfoWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use ContactInfoWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import ContactInfoWeb.ConnCase

      alias ContactInfoWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint ContactInfoWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(ContactInfo.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(ContactInfo.Repo, {:shared, self()})
    end

    {:ok,
     conn:
       Phoenix.ConnTest.build_conn()
       |> Plug.Conn.put_req_header(
         "authorization",
         "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJwYXJjLWNsaSIsInN1YiI6InRlc3RfdG9rZW4iLCJpYXQiOjE2MTA3MDE3Njh9.UYv-JA5_vxaQnG7uJF9oC2CLRfRjf3OtsDPCQzSZkn4jiiqkAbxnluMx7NMMC3RAfJ_OfBazxoQ4e11ALM7WVIDohV05YHsVnh5I3ELCk3yEWjODNNam83YPOkuUt-j3k8uqaMuiVmqq6w8nsVeLVMv_mr70R2WWeLw7Vhxyo7a8Zk4ZBsS8syn3CUN8voIYEGWEb-t-DEbkQ3SVSiVTzi8-QspqFzKXhQmz47AMWnbetgF0pRsQQfNAs2yf48SYcEzosPdLqcl5BzLg7nTqBe1z6smuRElVw_hTLuynalCrnOCMbDJupeOjCTWwa_D8dRdSCrLrE7kZRS3ZB8xyzg"
       )}
  end
end
