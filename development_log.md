## CI

.gitlab-ci is a bit of a pain.
Should really cache dependencies; Best way to do this would probably to keep a
separate docker image just for CI and just rebuild that whenever dependencies
change instead of trying to get the .gitlab-ci caching system to work.

Don't have the time to set this up right now.

Break
=====
10:30-13:00
