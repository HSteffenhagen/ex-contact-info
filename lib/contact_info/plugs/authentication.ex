defmodule ContactInfo.Plugs.Authentication do
  import Plug.Conn

  def init(_config) do
    Application.get_env(:contact_info, ContactInfoWeb.Endpoint)[:authentication]
  end

  def call(conn, config) do
    if config.enabled? do
      with {:ok, authorization_jwt} <- fetch_authorization_header(conn.req_headers),
           {:ok, authorization} <- ContactInfo.AuthToken.verify_and_validate(authorization_jwt) do
        conn |> assign(:jwt, authorization)
      else
        {:error, _} -> not_authorized(conn)
      end
    else
      conn
    end
  end

  defp fetch_authorization_header([{"authorization", authorization} | _]) do
    {:ok, authorization}
  end

  defp fetch_authorization_header([_ | headers]) do
    fetch_authorization_header(headers)
  end

  defp fetch_authorization_header([]) do
    {:error, :not_found}
  end

  defp not_authorized(conn) do
    conn
    |> put_resp_content_type("text/plain")
    |> resp(403, "forbidden")
    |> halt()
  end
end
