defmodule ContactInfo.Contact do
  use Ecto.Schema
  import Ecto.Changeset

  # The case_id could be a foreign key into a cases table;
  # Since we don't actually store any info about cases for now we'll just put it here directly.
  # Note that this does have a bit of memory overhead compared to having an extra cases table
  # If we had a 1000 cases with a 1000 contacts each we'd spend about 100 megabytes just storing case
  # ids, whereas we'd only need a bit over 8 if we had a separate cases table at the cost of making our
  # queries slightly more complicated (but probably also faster).
  # It's worth doing so I'll take a look at it if I still have the time later on.

  schema "contacts" do
    field :case_id, :string
    field :title, :string
    field :first_name, :string
    field :last_name, :string
    field :mobile_number, :string
    field :address, :string

    timestamps()
  end

  @doc false
  def changeset(contact, attrs \\ %{}) do
    contact
    |> cast(attrs, [:case_id, :title, :first_name, :last_name, :mobile_number, :address])
    # everything but case_id is optional
    |> validate_required([:case_id])
  end
end
