defmodule ContactInfo.AuthToken do
  use Joken.Config, default_signer: :rs256

  @impl true
  def token_config do
    # We don't do any special validation for now, and we don't generate claims
    %{}
  end
end
